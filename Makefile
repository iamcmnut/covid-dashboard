.PHONY: up
up: ## Spawning development environment.
up:
	@docker-compose up --build

.PHONY: down
down: ## Tearing down development environment.
down:
	@docker-compose down -v && docker rmi covid-dashboard_web

.PHONY: reset
reset: ## Resetting development environment.
reset:
	@make down || true
	@make up

