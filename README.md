# README

This README would normally document whatever steps are necessary to get the
application up and running.

# How to run
  1. Install Docker - https://www.docker.com/products/docker-desktop
  2. run `make up`

# Restting development
  1. run `make reset`

# Clean up development
  1. run `make down`

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
