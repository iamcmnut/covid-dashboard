# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_06_142933) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "covid_cases", force: :cascade do |t|
    t.integer "no"
    t.float "age"
    t.string "sex"
    t.string "nation"
    t.string "occ_new"
    t.string "province"
    t.text "risk"
    t.string "district"
    t.datetime "notification_date"
    t.datetime "announce_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "origin"
    t.string "province_isolation"
    t.index ["no"], name: "index_covid_cases_on_no", unique: true
  end

  create_table "timelines", force: :cascade do |t|
    t.datetime "activity_date"
    t.text "activity"
    t.string "coordinate"
    t.float "latitude"
    t.float "longitude"
    t.bigint "covid_case_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["covid_case_id"], name: "index_timelines_on_covid_case_id"
  end

  add_foreign_key "timelines", "covid_cases"
end
