class ChangeColumnTypeAgeToFloat < ActiveRecord::Migration[6.0]
  def change
    change_column :covid_cases, :age, :float
  end
end
