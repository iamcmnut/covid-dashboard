class AddIsolationProvinceToCovidCases < ActiveRecord::Migration[6.0]
  def change
    add_column :covid_cases, :province_isolation, :string
  end
end
