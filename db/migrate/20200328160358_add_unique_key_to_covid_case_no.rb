class AddUniqueKeyToCovidCaseNo < ActiveRecord::Migration[6.0]
  def change
    add_index :covid_cases, :no, unique: true
  end
end
