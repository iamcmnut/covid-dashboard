class CreateCovidCases < ActiveRecord::Migration[6.0]
  def change
    create_table :covid_cases do |t|
      t.integer :no
      t.integer :age
      t.string :sex
      t.string :nation
      t.string :occ_new
      t.string :province
      t.text :risk
      t.string :district
      t.datetime :notification_date
      t.datetime :announce_date

      t.timestamps
    end
  end
end
