class CreateTimelines < ActiveRecord::Migration[6.0]
  def change
    create_table :timelines do |t|
      t.datetime :activity_date
      t.text :activity
      t.string :coordinate
      t.float :latitude
      t.float :longitude
      t.references :covid_case, null: false, foreign_key: true

      t.timestamps
    end
  end
end
