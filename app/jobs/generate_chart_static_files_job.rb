class GenerateChartStaticFilesJob < ApplicationJob
  queue_as :default

  def perform(*args)
    cases = CovidCase.all
    gender_chart(cases)
    province_chart(cases)
    country_chart(cases)
    occupation_chart(cases)
    age_chart(cases)
    bkk_and_non_time_serie_chart(cases)
    country_without_thailand_chart(cases)
    province_without_bkk_chart(cases)
  end

  private
    def gender_chart(cases)
      genders = {"ชาย" => 0,"หญิง" => 0}
      cases.each do |c|
        if genders[c.sex].nil?
          genders[c.sex] = 1
        else
          genders[c.sex] = genders[c.sex] + 1
        end
      end
      genders_title = []
      genders_val = []
      genders.each do |key, val|
        genders_title.push key
        genders_val.push val
      end
      fin = "#{Rails.public_path}/charts/template/by-gender.js" 
      fout = "#{Rails.public_path}/charts/by-gender.js" 
      content = File.read(fin)
      content = content.sub!("$data", genders_val.inspect).sub!("$label", genders_title.inspect)
      File.open(fout, 'w') do |file|
        file.write(content)
      end
      puts "Generated: by-gender.js"
    end

    def province_chart(cases)
      label_01 = "กทม"
      label_02 = "ต่างจังหวัด"
      label_03 = "ไม่ระบุ"
      provinces = {label_01 => 0, label_02 => 0, label_03 => 0}
      cases.each do |c|
        if c.province == label_01
          provinces[label_01] = provinces[label_01] + 1
        elsif c.province != label_03
          provinces[label_02] = provinces[label_02] + 1
        else
          provinces[label_03] = provinces[label_03] + 1
        end
      end
      provinces_title = []
      provinces_val = []
      provinces.each do |key, val|
        provinces_title.push key
        provinces_val.push val
      end
      fin = "#{Rails.public_path}/charts/template/by-province.js" 
      fout = "#{Rails.public_path}/charts/by-province.js" 
      content = File.read(fin)
      content = content.sub!("$data", provinces_val.inspect).sub!("$label", provinces_title.inspect)
      File.open(fout, 'w') do |file|
        file.write(content)
      end
      puts "Generated: by-province.js"
    end

    def country_chart(cases)
      label_01 = "ไทย"
      label_02 = "ต่างชาติ"
      label_03 = "ไม่ระบุ"
      countries = {label_01 => 0, label_02 => 0, label_03 => 0}
      cases.each do |c|
        if c.nation == label_01
          countries[label_01] = countries[label_01] + 1
        elsif c.nation != label_03
          countries[label_02] = countries[label_02] + 1
        else
          countries[label_03] = countries[label_03] + 1
        end
      end
      countries_title = []
      countries_val = []
      countries.each do |key, val|
        countries_title.push key
        countries_val.push val
      end
      fin = "#{Rails.public_path}/charts/template/by-country-th-and-non.js" 
      fout = "#{Rails.public_path}/charts/by-country-th-and-non.js" 
      content = File.read(fin)
      content = content.sub!("$data", countries_val.inspect).sub!("$label", countries_title.inspect)
      File.open(fout, 'w') do |file|
        file.write(content)
      end
      puts "Generated: by-country-th-and-non.js"
    end

    def occupation_chart(cases)
      label_01 = "ไม่ระบุ"
      occ = {}
      occ_data = []
      occ_label = []
      cases.each do |c|
        if c.occ_new != label_01
          if occ[c.occ_new].nil?
            occ[c.occ_new] = 1
          else
            occ[c.occ_new] = occ[c.occ_new] + 1
          end
        end
      end
      occ = occ.sort_by {|_key, value| value}.reverse[..9].to_h
      occ.each do |key, val|
        occ_data.push(val)
        occ_label.push(key)
      end
      fin = "#{Rails.public_path}/charts/template/by-occupation.js" 
      fout = "#{Rails.public_path}/charts/by-occupation.js" 
      content = File.read(fin)
      content = content.sub!("$data", occ_data.inspect).sub!("$label", occ_label.inspect)
      File.open(fout, 'w') do |file|
        file.write(content)
      end
      puts "Generated: by-occupation.js"
    end

    def age_chart(cases)
      file_name = "by-age.js"
      ages = {
        "ชาย" => {
          "0-10" => 0,
          "11-20" => 0,
          "21-30" => 0,
          "31-40" => 0,
          "41-50" => 0,
          "51-60" => 0,
          "61-70" => 0,
          "71-80" => 0,
          "81-90" => 0,
          "91+" => 0
        },
        "หญิง" => {
          "0-10" => 0,
          "11-20" => 0,
          "21-30" => 0,
          "31-40" => 0,
          "41-50" => 0,
          "51-60" => 0,
          "61-70" => 0,
          "71-80" => 0,
          "81-90" => 0,
          "91+" => 0
        }
      }
      cases.each do |c|
        gender = c.sex
        if gender != "ไม่ระบุ" && c.age != nil
          if c.age >= 91
            ages[gender]["91+"] = ages[gender]["91+"] + 1
          elsif c.age >= 81
            ages[gender]["81-90"] = ages[gender]["81-90"] + 1
          elsif c.age >= 71
            ages[gender]["71-80"] = ages[gender]["71-80"] + 1
          elsif c.age >= 61
            ages[gender]["61-70"] = ages[gender]["61-70"] + 1
          elsif c.age >= 51
            ages[gender]["51-60"] = ages[gender]["51-60"] + 1
          elsif c.age >= 41
            ages[gender]["41-50"] = ages[gender]["41-50"] + 1
          elsif c.age >= 31
            ages[gender]["31-40"] = ages[gender]["31-40"] + 1
          elsif c.age >= 21
            ages[gender]["21-30"] = ages[gender]["21-30"] + 1
          elsif c.age >= 11
            ages[gender]["11-20"] = ages[gender]["11-20"] + 1
          elsif c.age >= 0
            ages[gender]["0-10"] = ages[gender]["0-10"] + 1
          end
        end
      end
      age_data_male = []
      age_data_female = []
      age_label = []

      ages["ชาย"].each do |key, val|
        age_data_male.push(val)
        age_label.push(key)
      end

      ages["หญิง"].each do |key, val|
        age_data_female.push(val)
      end

      fin = "#{Rails.public_path}/charts/template/#{file_name}" 
      fout = "#{Rails.public_path}/charts/#{file_name}" 
      content = File.read(fin)
      content = content.sub!("$data_male", age_data_male.inspect).sub!("$data_female", age_data_female.inspect).sub!("$label", age_label.inspect)
      File.open(fout, 'w') do |file|
        file.write(content)
      end
      puts "Generated: #{file_name}"
    end

    def bkk_and_non_time_serie_chart(cases)
      date_format = "dd/mm/yyyy"
      file_name = "by-bkk-and-nonbkk-time-serie.js"
      label_01 = "กทม"
      label_02 = "ต่างจังหวัด"
      label_03 = "ไม่ระบุ"

      date_min = CovidCase.minimum(:announce_date).to_date
      date_max = CovidCase.maximum(:announce_date).to_date
      
      date_tmp = date_min.clone
      date_diff = (date_max - date_min).to_i
      dates = {}

      for i in 0..date_diff
        dates[(date_tmp + i).strftime("%d/%m/%Y")] = {
          label_01 => 0,
          label_02 => 0,
          label_03 => 0
        }
      end

      cases.each do |c|
        d = c.announce_date.to_date.strftime("%d/%m/%Y")
        if c.province == label_01
          dates[d][label_01] = dates[d][label_01] + 1
        elsif c.province != label_03
          dates[d][label_02] = dates[d][label_02] + 1
        else
          dates[d][label_03] = dates[d][label_03] + 1
        end
      end

      yesterday = nil
      today = nil

      dates.each do |key,val|
        today = val
        if yesterday == nil
          yesterday = today.clone
        else
          yesterday[label_01] = yesterday[label_01] + today[label_01]
          yesterday[label_02] = yesterday[label_02] + today[label_02]
          yesterday[label_03] = yesterday[label_03] + today[label_03]
        end
        dates[key] = yesterday.clone
      end

      data_bkk = []
      data_non_bkk = []
      data_na = []

      dates.each do |key,val|
        data_bkk.push({"x" => key, "y" => val[label_01]})
        data_non_bkk.push({"x" => key, "y" => val[label_02]})
        data_na.push({"x" => key, "y" => val[label_03]})
      end
      

      fin = "#{Rails.public_path}/charts/template/#{file_name}" 
      fout = "#{Rails.public_path}/charts/#{file_name}" 
      content = File.read(fin)
      content = content.sub!("$data_bkk", data_bkk.to_json).sub!("$data_non_bkk", data_non_bkk.to_json).sub!("$data_na", data_na.to_json)
      File.open(fout, 'w') do |file|
        file.write(content)
      end
      puts "Generated: #{file_name}"
    end

    def country_without_thailand_chart(cases)
      file_name = "by-country-without-thailand.js"

      label_01 = "ไทย"
      label_02 = "ไม่ระบุ"

      nations = {}
      nations_data = []
      nations_label = []

      cases.each do |c|
        if c.nation != label_01 && c.nation != label_02
          if nations[c.nation].nil?
            nations[c.nation] = 1
          else
            nations[c.nation] = nations[c.nation] + 1
          end
        end
      end
      nations = nations.sort_by {|_key, value| value}.reverse[..9].to_h
      nations.each do |key, val|
        nations_data.push(val)
        nations_label.push(key)
      end
      fin = "#{Rails.public_path}/charts/template/#{file_name}" 
      fout = "#{Rails.public_path}/charts/#{file_name}" 
      content = File.read(fin)
      content = content.sub!("$data", nations_data.inspect).sub!("$label", nations_label.inspect)
      File.open(fout, 'w') do |file|
        file.write(content)
      end
      puts "Generated: #{file_name}"
    end

    def province_without_bkk_chart(cases)
      label_01 = "กทม"
      label_02 = "ไม่ระบุ"

      provinces = {}
      provinces_data = []
      provinces_label = []

      cases.each do |c|
        if c.province != label_01 && c.province != label_02
          if provinces[c.province].nil?
            provinces[c.province] = 1
          else
            provinces[c.province] = provinces[c.province] + 1
          end
        end
      end
      provinces = provinces.sort_by {|_key, value| value}.reverse[..9].to_h
      provinces.each do |key, val|
        provinces_data.push(val)
        provinces_label.push(key)
      end
      fin = "#{Rails.public_path}/charts/template/by-province-without-bkk.js" 
      fout = "#{Rails.public_path}/charts/by-province-without-bkk.js" 
      content = File.read(fin)
      content = content.sub!("$data", provinces_data.inspect).sub!("$label", provinces_label.inspect)
      File.open(fout, 'w') do |file|
        file.write(content)
      end
      puts "Generated: by-province-without-bkk.js"
    end
end
