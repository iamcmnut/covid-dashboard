class CovidCasesController < ApplicationController

  around_action :switch_locale

  def index
    I18n.default_locale = :th
    @display_chart = {
      :by_occupation => true,
      :by_age => true,
      :by_gender => true,
      :by_province => true
    }

    @menu = {"menu1" => "active"}

    @covid_cases = CovidCase.order(no: :desc).limit(10)
    @oldest = CovidCase.where("age >= 0").maximum(:age)
    @youngest = CovidCase.where("age >= 0").minimum(:age)
    @age_avg = CovidCase.where("age >= 0").average("age")
    @latest_date = CovidCase.order(announce_date: :desc).first
    @total_cases = CovidCase.maximum(:no)

    if @youngest < 1
      @youngest = (@youngest * 12)
    end
    
    @title = "สถิติภาพรวมของประเทศไทย"
    
    # render 'covid_cases/index', :locals => {@covid_cases => @covid_cases, @display_chart => @display_chart}
  end

  def new
  end

  def create

    @covid_case = CovidCase.new(user_params)

    @covid_case.save
    redirect_to @covid_case
  end

  def show
    @covid_case = CovidCase.find(params[:id])
  end

  def get_all_json
    @covid_cases = CovidCase.all
    render json: @covid_cases
  end

  private
    def user_params
      params.require(:covid_case).permit(:no, :age, :sex, :nation, :occ_new, :province, :risk, :district, :notification_date, :announce_date)
    end

    def switch_locale(&action)
      locale = params[:locale] || I18n.default_locale
      I18n.with_locale(locale, &action)
    end
end


# -- select occ_new, count(no) as total from covid_cases where occ_new <> 'ไม่ระบุ' group by occ_new order by count(no) desc limit 10;
# select province, count(no) as total from covid_cases where province <> 'ไม่ระบุ' group by province order by count(no) desc limit 10;