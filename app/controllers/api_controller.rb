class ApiController < ActionController::API
  def get_covid_cases
    @covid_cases = CovidCase.all
    render json: @covid_cases
  end
end