class AdminController < ApplicationController
  http_basic_authenticate_with name: ENV["RAILS_ADMIN_USERNAME"] ? ENV["RAILS_ADMIN_USERNAME"] : "dhh", password: ENV["RAILS_ADMIN_PASSWORD"] ? ENV["RAILS_ADMIN_PASSWORD"] : "secret"
  def index
    GenerateChartStaticFilesJob.perform_later
    render plain: "RUN"
  end
end
