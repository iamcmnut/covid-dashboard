Rails.application.routes.draw do
  get 'support/index'
  get 'timeline/index'
  get 'timelines', to: 'timeline#index'
  get 'admin', to: 'admin#index'
  get 'welcome/index'

  get 'api/get_all_json', to: 'api#get_covid_cases'
  # get 'covid_cases/get_all_json', to: 'covid_cases#get_all_json'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources :covid_cases
  resources :timelines

  root 'covid_cases#index'
end
