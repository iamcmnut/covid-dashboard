(() => {
  var canvas1 = document.getElementById('by-age-chart');
  if (canvas1) {

    var config = {
      type: 'bar',
      data: {
        datasets: [
          {
            data: $data_male,
            backgroundColor: [
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff'
            ],
            label: 'ชาย'
          },
          {
            data: $data_female,
            backgroundColor: [
              '#e83e8c',
              '#e83e8c',
              '#e83e8c',
              '#e83e8c',
              '#e83e8c',
              '#e83e8c',
              '#e83e8c',
              '#e83e8c',
              '#e83e8c',
              '#e83e8c'
            ],
            label: 'หญิง'
          }
        ],
        labels: $label
      },
      options: {
        responsive: true,
        legend: {
          labels: {
            // This more specific font property overrides the global property
            fontColor: 'black',
            defaultFontSize: 100
          }
        },
        scales: {
          xAxes: [{
            stacked: true
          }],
          yAxes: [{
            stacked: true
          }]
        }
      }
    };

    var chart1 = new Chart(canvas1, config);
  }
})();