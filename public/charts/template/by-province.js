(() => {
  var canvas1 = document.getElementById('by-province-chart');
  if (canvas1) {

    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: $data,
          backgroundColor: [
            '#dc3545',
            '#28a745',
            '#333'
          ],
          label: 'ข้อมูลผู้ป่วยแบ่งตามเพศ'
        }],
        labels: $label
      },
      options: {
        responsive: true
      }
    };

    var chart1 = new Chart(canvas1, config);
  }
})();