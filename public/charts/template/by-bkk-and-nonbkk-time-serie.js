(() => {
  var canvas1 = document.getElementById('by-bkk-and-nonbkk-time-serie');
  if (canvas1) {
    var timeFormat = 'DD/MM/YYYY';

    var config = {
      type: 'line',
      data: {
        datasets: [
          {
            label: 'กทม',
            backgroundColor: '#dc3545',
            fill: false,
            borderColor: '#dc3545',
            data: $data_bkk
          },
          {
            label: 'ต่างจังหวัด',
            backgroundColor: '#28a745',
            fill: false,
            borderColor: '#28a745',
            data: $data_non_bkk
          },
          {
            label: 'ไม่ระบุ',
            backgroundColor: '#333',
            fill: false,
            borderColor: '#333',
            data: $data_na
          }
        ]
      },
      options: {
        scales: {
          xAxes: [{
            type: 'time',
            time: {
              parser: timeFormat,
              // round: 'day'
              tooltipFormat: 'DD-MMM-YYYY'
            },
            scaleLabel: {
              display: true,
              labelString: 'Date'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'value'
            }
          }]
        }
      }
    };

    var chart1 = new Chart(canvas1, config);
  }
})();