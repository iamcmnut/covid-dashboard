(() => {
  var canvas1 = document.getElementById('by-country-th-and-non');
  if (canvas1) {

    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: $data,
          backgroundColor: [
            '#007bff',
            '#dc3545',
            '#333'
          ],
          label: 'ข้อมูลผู้ป่วยแบ่งตามเพศ'
        }],
        labels: $label
      },
      options: {
        responsive: true
      }
    };

    var chart1 = new Chart(canvas1, config);
  }
})();