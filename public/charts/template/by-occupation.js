(() => {
  var canvas1 = document.getElementById('by-occupation-chart');
  if (canvas1) {

    var config = {
      type: 'horizontalBar',
      data: {
        datasets: [
          {
            data: $data,
            backgroundColor: [
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
              '#007bff',
            ],
            label: 'จำนวนผู้ติดเชื้อ'
          }
        ],
        labels: $label
      },
      options: {
        responsive: true
      }
    };

    var chart1 = new Chart(canvas1, config);
  }
})();