(() => {
  var canvas1 = document.getElementById('by-province-without-bkk');
  if (canvas1) {

    var config = {
      type: 'horizontalBar',
      data: {
        datasets: [
          {
            data: $data,
            backgroundColor: [
              '#20c997',
              '#20c997',
              '#20c997',
              '#20c997',
              '#20c997',
              '#20c997',
              '#20c997',
              '#20c997',
              '#20c997',
              '#20c997',
            ],
            label: 'จำนวนผู้ติดเชื้อ'
          }
        ],
        labels: $label
      },
      options: {
        responsive: true
      }
    };

    var chart1 = new Chart(canvas1, config);
  }
})();